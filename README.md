# MediaWiki

> MediaWiki is an extremely powerful, scalable software and a feature-rich wiki implementation that uses PHP to process and display data stored in a database, such as MySQL.

> Yes, you can easily modify pages and you can (temporarily) publish dummy sentences, and you can even (temporarily) completely destroy a page in a wiki. You don't need to have any programming skills to do this.


## Installing Helm

* *curl -L https://git.io/get_helm.sh | bash*

* *kubectl create - f tiller-sa.yaml*        //Install Tiller on K8s cluster

* *helm init --service-account=tiller --history-max 300*         //Initialize Helm with tiller account 

* *kubectl get deployment tiller-deploy -n kube-system*


## Creating project

* *helm create mediawiki .*

* *helm dependency update*   //Create requirements.yaml for mysql dependency and then update dependency

* Create deployment.yaml for the application under the folder templates

* Create nodeport service and ingress for mediawiki by creating service.yaml and ingress.yaml under templates

* *helm install --name mediawiki .*

 
   <br /> NAME:   mediawiki
   <br /> LAST DEPLOYED: Thu Mar 12 18:29:56 2020
   <br /> NAMESPACE: default
   <br /> STATUS: DEPLOYED
   <br /> 
   <br /> RESOURCES:
   <br /> ==> v1/ConfigMap
   <br /> NAME                  AGE
   <br /> mediawiki-mysql-test  0s
   <br /> 
   <br /> ==> v1/Deployment
   <br /> NAME             AGE
   <br /> mediawiki        0s
   <br /> mediawiki-mysql  0s
   <br /> 
   <br /> ==> v1/PersistentVolumeClaim
   <br /> NAME             AGE
   <br /> mediawiki-mysql  0s
   <br /> wp-pvc-claim     0s
   <br /> 
   <br /> ==> v1/Pod(related)
   <br /> NAME                              AGE
   <br /> mediawiki-d4d579c59-pjqm5         0s
   <br /> mediawiki-mysql-676479c565-hg6rp  0s
   <br /> 
   <br /> ==> v1/Secret
   <br /> NAME             AGE
   <br /> mediawiki-mysql  0s
   <br /> mysql-pass       0s
   <br /> 
   <br /> ==> v1/Service
   <br /> NAME             AGE
   <br /> mediawiki        0s
   <br /> mediawiki-mysql  0s
   <br /> 
   <br /> ==> v1/ServiceAccount
   <br /> NAME       AGE
   <br /> mediawiki  0s
   <br /> 
   <br /> ==> v1beta1/Ingress
   <br /> NAME               AGE
   <br /> mediawiki-ingress  0s
   <br /> 
   <br /> 
   <br /> NOTES:
   <br /> 1. Get the application URL by running these commands:
   <br />   http://testwiki.info/

* *helm ls*
   <br />Output is
   <br />NAME     	REVISION	UPDATED                 	STATUS  	CHART          	APP VERSION	NAMESPACE
   <br />mediawiki	1       	Mon Mar 16 16:23:58 2020	DEPLOYED	mediawiki-0.1.0	1.0        	default  


* Verify the IP address is set by running command
   <br />*kubectl get ingress*
   <br /> Output will be 

   <br />NAME                HOSTS           ADDRESS     PORTS   AGE
   <br />mediawiki-ingress   testwiki.info   10.0.2.15   80      98s

* Get the minikube ipby running command *minikube ip host*
   <br /> Output will be <minikube ip>
   <br /> Example : 192.168.99.107

* Add the line **<minikube ip> <ingress host>** to the bottom of the /etc/hosts file by running command *sudo nano /etc/hosts*-
   <br />**192.168.99.107 testwiki.info**


* Verify that the Ingress controller is directing traffic:

   <br />*curl testwiki.info*
   <br /> Output will be the mediawiki page

## Setting up the WIKI

* set up the mysql database by specifying
      <br />Database host: mediawiki-mysql
      <br />Database name: my-wiki
      <br />Database username: root
      <br />Database password: password

* Specify wiki credentials and set the email and password

* Download the LocalSettings.php and copy it to the container at /var/www/html
    <br /> *kubectl cp LocalSettings.php mediawiki-786bd6fc77-fjq5v:/var/www/html*

* Refresh your wiki url to enter the Main Index page and add content.

* For different environments,use values.yaml fromthe config folder. 
      <br />UAT - *helm install -f ./config/values_production.yaml --name  mediawiki .* 
      <br />PROD - *helm install -f ./config/values_production.yaml --name  mediawiki .*
